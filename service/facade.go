package service

import (
	"context"
	"gobff"
	"log"
	"net/http"
	"time"
)

type integration struct {
	att  string
	geom string
}

var environment = map[string]integration{
	"local": {
		att:  "http://localhost:3005/foo",
		geom: "http://localhost:3006/bar",
	},
	"mocky": {
		att:  "https://run.mocky.io/v3/4c3e984c-b48e-46b9-b77e-8d4d10d4d5c3",
		geom: "https://run.mocky.io/v3/99c1036a-6bfb-4346-8fc8-0313d62239f8",
	},
}

type attribute struct {
	Name       string `json:"name"`
	LaunchDate string `json:"launch_date"`
	OpenHour   string `json:"open_hour"`
}

type geometry struct {
	Geom string `json:"geometry"`
}

type Facade struct {
}

func NewService() *Facade {
	return &Facade{}
}

func (s *Facade) Region(ctx context.Context, id int) (*gobff.Region, error) {
	env := ctx.Value("env").(string)

	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	errCh := make(chan error, 1)

	// start attributes goroutine.
	attReq, _ := http.NewRequestWithContext(ctx, http.MethodGet, environment[env].att, nil)
	attCh := make(chan attribute, 1)
	go request(attCh, errCh, attReq)

	// start geometry goroutine.
	geomReq, _ := http.NewRequestWithContext(ctx, http.MethodGet, environment[env].geom, nil)
	geomCh := make(chan geometry, 1)
	go request(geomCh, errCh, geomReq)

	// err chan will receive two values, one for each goroutine
	// so we have to read the channel more then once.
	if err := <-errCh; err != nil {
		return nil, err
	}

	if err := <-errCh; err != nil {
		return nil, err
	}

	l := <-attCh
	a := <-geomCh

	return &gobff.Region{
		Name:       l.Name,
		LaunchDate: l.LaunchDate,
		LaunchHour: l.OpenHour,
		Geometry:   a.Geom,
	}, nil
}

func request[T any](c chan T, chErr chan error, req *http.Request) {
	resp, err := do[T](req)
	if err != nil {
		chErr <- err
		return
	}
	log.Printf("called request %s\n", req.URL)
	c <- resp
	chErr <- nil
}
