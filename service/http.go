package service

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func do[T interface{}](req *http.Request) (T, error) {
	client := &http.Client{}
	var result T

	resp, err := client.Do(req)

	if err != nil {
		return result, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		if err := json.Unmarshal(body, &result); err != nil {
			return result, err
		}

		return result, nil
	} else if resp.StatusCode >= 400 && resp.StatusCode <= 499 {
		return result, fmt.Errorf("not found [statusCode: %d]", resp.StatusCode)
	} else {
		var errorResponse map[string]interface{}

		if err := json.Unmarshal(body, &errorResponse); err != nil {
			return result, err
		}

		return result, fmt.Errorf("error when calling API: [statusCode: %d, body: %v]", resp.StatusCode, errorResponse)
	}
}
