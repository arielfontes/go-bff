package gobff

import "context"

type Region struct {
	Name       string
	LaunchDate string
	LaunchHour string
	Geometry   string
}

type RegionService interface {
	Region(ctx context.Context, id int) (Region, error)
}
