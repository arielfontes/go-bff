package main

import (
	"context"
	"encoding/json"
	"gobff/service"
	"log"
	"net/http"
)

func main() {
	service := service.NewService()

	http.HandleFunc("/region", func(w http.ResponseWriter, r *http.Request) {
		env := r.Header.Get("env")

		if env == "" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(r.Context(), "env", env)

		region, err := service.Region(ctx, 1)
		if err != nil {
			log.Println("get region", err)
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(region)
	})

	http.ListenAndServe(":9000", nil)
}
